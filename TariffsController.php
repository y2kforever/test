<?php
/**
 * Created by PhpStorm.
 * User: abdugania
 * Date: 14/10/18
 * Time: 12:55 AM
 */

class TariffsController extends Controller
{

    private $service;

    public function __construct($id, $module, TariffManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }


    public function actionCreate()
    {
        $form = new TariffForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $this->service->create($form);
                $transaction->commit();
                Yii::$app->session->setFlash('success', 'Тариф создан.');
            } catch (\Exception $exception) {
                $transaction->rollBack();
                Yii::warning($exception, 'subject/tariffs/create');
                Yii::$app->session->setFlash('error', $exception->getMessage());
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $form,
            ]);
        }
        return $this->render('create', [
            'model' => $form,
        ]);
    }

}