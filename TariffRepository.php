<?php

class TariffRepository
{

    public function get($id): Tariffs
    {
        if (!$tariff = Tariffs::findOne(['id' => $id])) {
            throw new NotFoundException('Tariff is not found.');
        }
        return $tariff;
    }

    public function save(Tariffs $tariff): void
    {
        if (!$tariff->save(false)) {
            throw new \RuntimeException(Yii::t('Subject', 'TARIFF_SAVE_ERROR'));
        }
    }

    public function remove(Tariffs $tariff): void
    {
        if (!$tariff->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

}