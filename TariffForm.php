<?php
/**
 * Created by PhpStorm.
 * User: abdugania
 * Date: 14/10/18
 * Time: 12:58 AM
 */

/**
 * @property FleetTariffsForm fleetsTariffs
 */

class TariffForm extends CompositeForm
{
    public $id;
    public $name_ru;
    public $name_en;
    public $desc_ru;
    public $desc_en;
    public $extra_desc_ru;
    public $extra_desc_en;
    public $amount;
    public $alias;
    public $push_color;
    public $state_id;
    public $url;
    ...

    public function __construct(Tariffs $tariff = null, $config = [])
    {
        if ($tariff) {
            $this->id = $tariff->id;
            $this->name_ru = $tariff->name_ru;
            $this->name_en = $tariff->name_en;
            $this->desc_ru = $tariff->desc_ru;
            $this->desc_en = $tariff->desc_en;
            $this->extra_desc_ru = $tariff->extra_desc_ru;
            $this->extra_desc_en = $tariff->extra_desc_en;
            $this->amount = $tariff->summa;
            $this->alias = $tariff->alias;
            $this->push_color = $tariff->push_color;
            $this->state_id = $tariff->state_id;
            $this->url = $tariff->url;
            ...
            $this->fleetsTariffs = new FleetTariffsForm($tariff->fleetsTariffs[0]);
            $this->fleetsTariffs->class_id = $tariff->id;
        } else {
            $this->fleetsTariffs = new FleetTariffsForm();
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return RULES...;
    }

    protected function internalForms(): array
    {
        return ['fleetsTariffs'];
    }

    public function attributeLabels()
    {
        return LABELS...;
    }

}