<?php
/**
 * Created by PhpStorm.
 * User: abdugania
 * Date: 14/10/18
 * Time: 12:57 AM
 */

class TariffManageService
{

    public $tariff;
    public $fleet_tariff;
    public $payment_type;
    public $initiator;

    public function __construct(
        TariffRepository $tariff,
        FleetTariffsManageService $fleet_tariff,
        TariffPaymentTypesManageService $payment_type,
        TariffByInitiatorsManageService $initiator
    )
    {
        $this->tariff = $tariff;
        $this->fleet_tariff = $fleet_tariff;
        $this->payment_type = $payment_type;
        $this->initiator = $initiator;
    }

    public function create(TariffForm $tariffs): void
    {
        $tariff = Tariffs::create($tariffs);
        $this->tariff->save($tariff);
        $tariffs->fleetsTariffs->class_id = $tariff->id;
        $this->fleet_tariff->create($tariffs->fleetsTariffs);
        $this->payment_type->create($tariffs->fleetsTariffs->class_id, $tariffs->payment_types);
        $this->initiator->create($tariffs->fleetsTariffs->class_id, $tariffs->initiators);
    }

...

}